from flask import Flask, render_template, url_for


app = Flask(__name__)  

name = "John"
num_rows = 10
num_del = 3



def generate_measured_values(num_rows, num_del):
    
    measured_values = [
        {"timestamp": "12:00", "temp": 25},
        {"timestamp": "11:30", "temp": 24},
        {"timestamp": "11:00", "temp": 26},
        {"timestamp": "10:30", "temp": 24},
        {"timestamp": "10:00", "temp": 25},
        {"timestamp": "9:30", "temp": 18},
        {"timestamp": "9:00", "temp": 13},
        {"timestamp": "8:30", "temp": 12},
        {"timestamp": "8:00", "temp": 12},
        {"timestamp": "7:30", "temp": 10},
    ]
    
    generated_values = []

    for i in range(num_rows):
        if i < num_rows - num_del:
            timestamp = measured_values[i]["timestamp"]
            temp = measured_values[i]["temp"]
            generated_values.append({"timestamp": timestamp, "temp": temp})
        else:
            timestamp = "--"
            temp = "--"
            generated_values.append({"timestamp": timestamp, "temp": temp})

    return generated_values


 
@app.route('/')   
def dashboard():  
    measured_values = generate_measured_values(num_rows, num_del)
    return render_template('dashboard.html', name = name, measured_values=measured_values) 



@app.route('/login')   
def login():  
        
    return render_template('login.html', name = name)  


@app.route('/register')   
def register():  

    return render_template('register.html', name = name)   


 
if __name__ == "__main__":  
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=False)

